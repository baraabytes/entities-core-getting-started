﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesCoreStarted.Domain;

namespace EntitiesCoreStarted.Data
{
    public class AppDbContext:DbContext
    {
        public DbSet<Samurai> Samurais { get; set; }
        public DbSet<Quote> Quotes { set; get; }
        public DbSet<Battle> Battles { set; get; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string conn = @"Server=BARAAABUZAIFBD4\SQLEXPRESS;Database=SamuraiAppData;Trusted_Connection=true";
            optionsBuilder.UseSqlServer(conn);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
