﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesCoreStarted.Test
{
    class Class1
    {
        public static void CalculateWholeAnnualEntitlementByDayCalc(
    LeaveType leaveTypeItem,
    LeaveServiceCalculator serviceCalculator,
    out IntegerNameDecimalValueCollection entitlements,
    out IntegerNameDecimalValueCollection earns,
    ref decimal carryForwardEntitlement,
    RoundingSetup roundSetup,
    LeaveRoundingListSet leaveRoundings,
    ref string entitlementCalculationValues,
    ref bool annualLeaveCheck,
    ref string leaveEntitlementTable)
        {
            entitlements = new IntegerNameDecimalValueCollection();
            earns = new IntegerNameDecimalValueCollection();

            _annualCheck = annualLeaveCheck;

            if (null == roundSetup || null == leaveRoundings)
            {
                return;
            }

            var openServiceMonth = serviceCalculator.CurrentOpeningServiceMonthCount;
            var closeServiceMonth = serviceCalculator.CurrentClosingServiceMonthCount;
            var totalLeaveEarn = 0m;
            var previousCloseServiceMonthEntitlement = 0m;

            #region Determine actual entt by months

            for (int i = 0; i < 12; i++)
            {
                if (leaveTypeItem.IsProrate &&
                    12 >= closeServiceMonth && (int)(12 - closeServiceMonth) >= i)
                {
                    entitlements.Add(i, 0);
                }
                else
                {
                    for (int j = 0; j <= 10; j++)
                    {
                        if (10 == j)
                        {
                            entitlements.Add(i, 0);
                        }
                        else if (openServiceMonth + i <= leaveTypeItem.ToServiceMonthNumbers[j])
                        {
                            entitlements.Add(i, leaveTypeItem.Entitlements[j]);
                            break;
                        }
                    }
                }
            }
            if (_annualCheck)
            {
                leaveEntitlementTable =
                    GenerateLeaveEntitlementTable(
                        leaveTypeItem.ToServiceMonthNumbers,
                        leaveTypeItem.Entitlements);
            }

            #endregion Determine actual entt by months

            _entitlementMonthlyDecimalStr = string.Empty;

            if (leaveTypeItem.IsProrate)
            {
                #region Prorate

                #region Get just one Entt Figure (prevCloseSvcMonthEntt) for current year opening service month

                if (closeServiceMonth - openServiceMonth < 12)
                {
                    previousCloseServiceMonthEntitlement = 0;
                }
                else
                {
                    for (int j = 0; j <= 10; j++)
                    {
                        if (10 == j)
                        {
                            previousCloseServiceMonthEntitlement = 0;
                        }
                        else if (openServiceMonth <= leaveTypeItem.ToServiceMonthNumbers[j])
                        {
                            previousCloseServiceMonthEntitlement = leaveTypeItem.Entitlements[j];
                            break;
                        }
                    }
                }

                #endregion Get just one Entt Figure (prevCloseSvcMonthEntt) for current year opening service month

                int newActualYearMonth = 0, oldActualYearMonth = 0;
                decimal oldMonthEntt = 0, newMonthEntt = 0;
                decimal oldMonthAccuEarn = 0, newMonthAccuEarn = 0;
                int startDayNo = serviceCalculator.FirstOpenBalanceDate.Day;
                DateTime fiscalYTD = DateTime.MinValue;

                #region Determine baseStartCalcDate

                DateTime baseStartCalcDate = DateTime.MinValue;

                if (serviceCalculator.FirstOpenBalanceDate > serviceCalculator.CurrentOpenBalanceDate)
                {
                    baseStartCalcDate = serviceCalculator.FirstOpenBalanceDate;
                }
                else
                {
                    baseStartCalcDate = serviceCalculator.CurrentOpenBalanceDate;
                }

                #endregion Determine baseStartCalcDate

                decimal varianceForOldNewEntt = 0;

                for (int enttMonthNoIndex = 1; enttMonthNoIndex <= entitlements.Count; enttMonthNoIndex++)
                {
                    string tempStr = "", tempStr2 = "", currentMonth = "", currentMonthShort = "", previousMonth = "";
                    if ((1 == enttMonthNoIndex && previousCloseServiceMonthEntitlement != entitlements[enttMonthNoIndex - 1]))
                    {
                        oldMonthEntt = previousCloseServiceMonthEntitlement;
                        newMonthEntt = entitlements[enttMonthNoIndex - 1];
                    }
                    else if (enttMonthNoIndex > 1 && entitlements[enttMonthNoIndex - 1] != entitlements[enttMonthNoIndex - 2])
                    {
                        oldMonthEntt = entitlements[enttMonthNoIndex - 2];
                        newMonthEntt = entitlements[enttMonthNoIndex - 1];
                    }
                    else
                    {
                        oldMonthEntt = newMonthEntt = entitlements[enttMonthNoIndex - 1];
                    }

                    if (_annualCheck)
                    {
                        tempStr += leaveTypeItem.LeaveGroupId + "#";
                        tempStr += "" + _monthStrArrLong[enttMonthNoIndex - 1] + "#";
                        tempStr += newMonthEntt.ToString("0.00") + "#";
                    }

                    newActualYearMonth = CommonMethod.GetYearMonthValue(serviceCalculator.CurrentOpenBalanceDate.AddMonths(enttMonthNoIndex - 1));
                    oldActualYearMonth = CommonMethod.GetYearMonthValue(serviceCalculator.CurrentOpenBalanceDate.AddMonths(enttMonthNoIndex - 2));
                    currentMonth = serviceCalculator.CurrentOpenBalanceDate.AddMonths(enttMonthNoIndex - 1).ToString("MMMM");
                    currentMonthShort = serviceCalculator.CurrentOpenBalanceDate.AddMonths(enttMonthNoIndex - 1).ToString("MMM");
                    previousMonth = serviceCalculator.CurrentOpenBalanceDate.AddMonths(enttMonthNoIndex - 2).ToString("MMM");

                    if (oldMonthEntt == newMonthEntt)
                    {
                        fiscalYTD =
                            CommonMethod.GetEndOfMonthDate(
                            (int)(newActualYearMonth / 100), newActualYearMonth % 100);

                        // 20100303 CH 4.22.201 - use GetNumberOfDays instead of GetDayDifferent

                        DateTime tempfiscalYTD = fiscalYTD,
                            tempBaseStartCalcDate = baseStartCalcDate;

                        newMonthAccuEarn = newMonthEntt *
                            CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) /
                            CommonMethod.GetDayCountInYear(fiscalYTD.Year);

                        int tempNoOfDays = CommonMethod.GetNumberOfDays(tempBaseStartCalcDate, tempfiscalYTD),
                            tempDayCountInYear = CommonMethod.GetDayCountInYear(tempfiscalYTD.Year);

                        tempStr2 += "" + newMonthAccuEarn.ToString("0.000") + "#";

                        newMonthAccuEarn =
                            LeaveMethod.RoundLeaveFigure(
                            CommonMethod.RoundAmount(newMonthAccuEarn, roundSetup.Digit,
                            roundSetup.RoundingMethodEnum), ref leaveRoundings);

                        tempStr2 += "" + newMonthAccuEarn.ToString("0.000") + "#";

                        fiscalYTD = CommonMethod.GetEndOfMonthDate(
                            (int)(oldActualYearMonth / 100), oldActualYearMonth % 100);

                        if (baseStartCalcDate < fiscalYTD)
                        {
                            oldMonthAccuEarn = oldMonthEntt *
                                CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) /
                                CommonMethod.GetDayCountInYear(fiscalYTD.Year);
                            oldMonthAccuEarn -= varianceForOldNewEntt;
                        }

                        oldMonthAccuEarn = LeaveMethod.RoundLeaveFigure(
                            CommonMethod.RoundAmount(oldMonthAccuEarn, roundSetup.Digit,
                            roundSetup.RoundingMethodEnum), ref leaveRoundings);

                        if (oldMonthAccuEarn > newMonthAccuEarn)
                            oldMonthAccuEarn = 0;

                        earns.Add(enttMonthNoIndex - 1, newMonthAccuEarn - oldMonthAccuEarn);
                        varianceForOldNewEntt = 0;

                        decimal tempEarn = newMonthAccuEarn - oldMonthAccuEarn;

                        tempStr += (newMonthAccuEarn - oldMonthAccuEarn).ToString("0.000") + "#";
                        tempStr += oldMonthAccuEarn.ToString("0.000") + "#";
                        tempStr += tempStr2;
                        tempStr += tempNoOfDays.ToString() + "#";
                        tempStr += tempDayCountInYear.ToString() + "#";
                        tempStr += currentMonth + "#";
                        tempStr += currentMonthShort + "#";
                        tempStr += previousMonth + "#";
                    }
                    else
                    {
                        decimal earn1 = 0, earn2 = 0;
                        varianceForOldNewEntt = 0;
                        // calculate old entt part for current fiscal month

                        fiscalYTD = new DateTime((int)(newActualYearMonth / 100), newActualYearMonth % 100, startDayNo);
                        fiscalYTD = fiscalYTD.AddDays(-1);

                        // 20100303 CH 4.22.201
                        // formula changed: the NewMonthAccuEarn for old and new entt part of the current month should be added together first before rounding.

                        earn1 = oldMonthEntt * CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) / CommonMethod.GetDayCountInYear(fiscalYTD.Year);
                        //						newMonthAccuEarn = oldMonthEntt * CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) / CommonMethod.GetDayCountInYear(fiscalYTD.Year);

                        //						newMonthAccuEarn = LeaveMethod.RoundLeaveFigure(CommonMethod.RoundAmount(newMonthAccuEarn, rndSetup.Digit, rndSetup.RoundingMethodEnum), ref roundings);

                        fiscalYTD = CommonMethod.GetEndOfMonthDate((int)(oldActualYearMonth / 100), oldActualYearMonth % 100);

                        if (baseStartCalcDate < fiscalYTD)
                            oldMonthAccuEarn = oldMonthEntt * CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) / CommonMethod.GetDayCountInYear(fiscalYTD.Year);

                        oldMonthAccuEarn = LeaveMethod.RoundLeaveFigure(CommonMethod.RoundAmount(oldMonthAccuEarn, roundSetup.Digit, roundSetup.RoundingMethodEnum), ref leaveRoundings);

                        //						earn1 = newMonthAccuEarn - oldMonthAccuEarn;

                        // calculate new entt part for current fiscal month
                        // using new Entt Start Date
                        fiscalYTD = new DateTime((int)(newActualYearMonth / 100), newActualYearMonth % 100, startDayNo);
                        baseStartCalcDate = fiscalYTD;

                        //						oldMonthAccuEarn = 0;

                        fiscalYTD = CommonMethod.GetEndOfMonthDate((int)(newActualYearMonth / 100), newActualYearMonth % 100);

                        earn2 = newMonthEntt * CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) / CommonMethod.GetDayCountInYear(fiscalYTD.Year);
                        //						newMonthAccuEarn = newMonthEntt * CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD) / CommonMethod.GetDayCountInYear(fiscalYTD.Year);

                        int tempNumberofDays = CommonMethod.GetNumberOfDays(baseStartCalcDate, fiscalYTD),
                            tempNumberofDaysYear = CommonMethod.GetDayCountInYear(fiscalYTD.Year);

                        newMonthAccuEarn = earn1 + earn2;

                        tempStr2 += newMonthAccuEarn.ToString("0.000") + "#"; //Before round

                        newMonthAccuEarn = LeaveMethod.RoundLeaveFigure(CommonMethod.RoundAmount(newMonthAccuEarn, roundSetup.Digit, roundSetup.RoundingMethodEnum), ref leaveRoundings);

                        tempStr2 += newMonthAccuEarn.ToString("0.000") + "#"; //After round

                        //						earn2 = newMonthAccuEarn - oldMonthAccuEarn;
                        //varianceForOldNewEntt = [20 x 1jan8jun / 365] + [22x 9jun - 30jun / 365] - round([20 x 1jan8jun/365] + [22x 9jun-30jun/365]);
                        varianceForOldNewEntt = earn1 + earn2 - newMonthAccuEarn;

                        earns.Add(enttMonthNoIndex - 1, newMonthAccuEarn - oldMonthAccuEarn);
                        //earns.Add(enttMonthNoIndex - 1, round([20 x 1jan8jun/365] + [22x 9jun-30jun/365]) - (20 x 1jan8jun/365));
                        //						lvEarns.Add(fiscalMonthNo - 1, earn1 + earn2);

                        tempStr += (newMonthAccuEarn - oldMonthAccuEarn).ToString("0.000") + "#";
                        tempStr += oldMonthAccuEarn.ToString("0.000") + "#";
                        tempStr += tempStr2;
                        tempStr += tempNumberofDays.ToString() + "#";
                        tempStr += tempNumberofDaysYear.ToString() + "^" + earn1.ToString("0.000") + "#";
                        tempStr += currentMonth + "#";
                        tempStr += currentMonthShort + "#";
                        tempStr += previousMonth + "#";
                    }
                    if (_annualCheck)
                    {
                        _entitlementMonthlyDecimalStr += tempStr + ",";
                    }
                }

                var rdm = CommonMethod.RoundAmount(earns.Sum, roundSetup.Digit, roundSetup.RoundingMethodEnum);

                totalLeaveEarn = LeaveMethod.RoundLeaveFigure(rdm, ref leaveRoundings);

                earns.Clear();

                bool earnAssigned = false;

                for (int fiscalMonthNo = 1; fiscalMonthNo <= entitlements.Count; fiscalMonthNo++)
                {
                    if (entitlements[fiscalMonthNo - 1].Equals(0))
                    {
                        earns.Add(fiscalMonthNo - 1, 0);
                        continue;
                    }

                    if (!earnAssigned)
                    {
                        earns.Add(fiscalMonthNo - 1, totalLeaveEarn);
                        earnAssigned = true;
                    }
                    else
                    {
                        earns.Add(fiscalMonthNo - 1, 0);
                    }

                    entitlements[fiscalMonthNo - 1] = totalLeaveEarn;
                }

                #endregion Prorate
            }
            else
            {
                #region Not Prorate

                decimal currEarn = 0;

                for (int i = 0; i < entitlements.Count; i++)
                {
                    if (i.Equals(0))
                    {
                        currEarn = entitlements[i];
                    }
                    else
                    {
                        currEarn = entitlements[i] - entitlements[i - 1];
                    }

                    earns.Add(i, currEarn);
                }

                #endregion Not Prorate
            }

            if (0 < openServiceMonth)
            {
                for (int j = 0; j <= 10; j++)
                {
                    if (10 == j)
                    {
                        carryForwardEntitlement = 0;
                    }
                    else if ((openServiceMonth - 1) <= leaveTypeItem.ToServiceMonthNumbers[j])
                    {
                        carryForwardEntitlement = leaveTypeItem.CarriedForwardLimits[j];
                        break;
                    }
                }
            }
            if (_annualCheck && leaveTypeItem.IsProrate)
            {
                entitlementCalculationValues = _entitlementMonthlyDecimalStr.Substring(0, _entitlementMonthlyDecimalStr.Length - 1);
            }
        }
    }
}
